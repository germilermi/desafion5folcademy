import { Component, OnInit } from '@angular/core';
import { MoviesSeries } from 'src/interfaces/MoviesSeries';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  Category: string = "Todos";
  filter: string = "todos";
  name: any;


  movies_series: MoviesSeries[] = [
    {
      id: 1,
      name: "Black Widow",
      description: "Natasha Romanoff, alias Viuda Negra, se enfrenta a las partes más oscuras de su historia cuando surge una peligrosa conspiración con vínculos con su pasado. Perseguida por una fuerza que no se detendrá ante nada para acabar con ella, Natasha debe enfrentarse a su historia como espía y a las relaciones rotas que dejó a su paso mucho antes de convertirse en una Vengadora.",
      image: "../assets/cards-image/img1.png",
      rating: 7.5,
      category: "Pelicula"
    },
    {
      id: 2,
      name: "Shang Chi",
      description: "Adaptación cinematográfica del héroe creado por Steve Englehart y Jim Starlin en 1973, un personaje mitad chino, mitad americano, cuyo característico estilo de combate mezclaba kung-fu, nunchacos y armas de fuego.",
      image: "../assets/cards-image/img2.png",
      rating: 7.8,
      category: "Pelicula"
    },
    {
      id: 3,
      name: "Loki",
      description: "La nueva serie Loki, de Marvel Studios, empieza allí donde terminó Vengadores: Endgame. En ella, el voluble villano Loki vuelve a ganarse el apodo de Dios del Engaño.",
      image: "../assets/cards-image/img3.png",
      rating: 8.2,
      category: "Pelicula"
    },
    {
      id: 4,
      name: "How I Met Your Mother",
      description: "Natasha Romanoff, alias Viuda Negra, se enfrenta a las partes más oscuras de su historia cuando surge una peligrosa conspiración con vínculos con su pasado. Perseguida por una fuerza que no se detendrá ante nada para acabar con ella, Natasha debe enfrentarse a su historia como espía y a las relaciones rotas que dejó a su paso mucho antes de convertirse en una Vengadora.",
      image: "../assets/cards-image/img4.png",
      rating: 7.5,
      category: "Pelicula"
    },
    {
      id: 5,
      name: "Peaky Blinders",
      description: "En Gran Bretaña, Reino Unido se recuperan de la desesperación de la Gran Guerra, las personas sobreviven a como pueden, y las bandas criminales proliferan en una nación sacudida económicamente.",
      image: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/dYAeYkRqDfQrMGHdY7hrtllSPTu.jpg",
      rating: 8.6,
      category: "Serie"
    },
    {
      id: 6,
      name: "How to Sell Drugs Online (Fast) ",
      description: "¿Cómo recuperas a tu novia del traficante de drogas de la escuela? Para Moritz, la respuesta es clara: vender mejores medicamentos. Fuera de su dormitorio de adolescente, une fuerzas con su mejor amigo Lenny para lanzar lo que se convierte en un mercado de drogas en línea inesperadamente exitoso.",
      image: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/szk2hXe7etcSgyp6WxXCW1an301.jpg",
      rating: 8.3,
      category: "Serie"
    },
    {
      id: 7,
      name: "Ozark",
      description: "Un asesor financiero arrastra a su familia de Chicago a Missouri Ozarks, donde debe lavar 500 millones de dólares en cinco años para apaciguar a un capo de la droga.",
      image: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/m73bD8VjibSKuTWg597GQVyVhSb.jpg",
      rating: 8.2,
      category: "Serie"
    },
    {
      id: 8,
      name: "Naruto",
      description: "En otro mundo, los ninjas son el máximo poder, y en Village Hidden in the Leaves vive el ninja más sigiloso de la tierra. Doce años antes, el temible zorro de nueve colas aterrorizó al pueblo y se cobró muchas vidas antes de que fuera sometido y su espíritu sellado dentro del cuerpo de un bebé.",
      image: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/vauCEnR7CiyBDzRCeElKkCaXIYu.jpg",
      rating: 8.4,
      category: "Serie"
    }
    ]

    
  constructor() { }

  ngOnInit(): void {
    this.filter = 'Todos'
  }
  clickTodos() {
    this.filter = 'Todos'
  }
  clickPeliculas () {
    this.filter = 'Pelicula';
    this.Category = 'Pelicula';
  }
  
  clickSeries () {
    this.filter = 'Serie';
    this.Category = 'Serie';
  }

  Search() {
    if(this.name == "") {
      this.ngOnInit();
    }else{
      this.movies_series = this.movies_series.filter( res => {
        return res.name.toLocaleLowerCase().match(this.name.toLocaleLowerCase());
      });
    }
          }
  }
